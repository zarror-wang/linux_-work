#include "useritem.h"
#include "ui_useritem.h"
#include "friend_info_dia.h"
#include "client_sp2.h"
#include "client_sp1.h"
#include <QPicture>
#include <QDebug>

useritem::useritem(QWidget *parent ,QString u_name,QString u_id,QString clienter_name,QString clienter_id,QTcpSocket* c_tcp) :
    QWidget(parent),
    u_name(u_name),
    u_id(u_id),
    clienter_name(clienter_name),
    clienter_id(clienter_id),
    c_tcp(c_tcp),
    ui(new Ui::useritem)
{
    ui->setupUi(this);
}

useritem::~useritem()
{
    delete ui;
}

void useritem::on_to_info_detial_clicked()
{
    //转到好友详细信息页面
    //@todo:在这里从数据库获取详细的好友信息
    friend_info_dia * friend_info=new friend_info_dia(nullptr,u_id,u_name,nullptr);
    //调用函数填充
    friend_info->show();
}

void useritem::init_useritem(QImage * user_icon){
    ui->name_val->setText(u_name);
    ui->user_icon->setPixmap(QPixmap::fromImage(*user_icon));
}

void useritem::on_to_meg_page_clicked()
{
    //断开sp1的tcp链接
    c_tcp->disconnect();
    //转到详细的聊天页面
    client_sp2* sp2=new client_sp2(nullptr,u_id,u_name,clienter_id,clienter_name,c_tcp);
    sp2->gen_req4_meglog();
    sp2->show();
    //退出sp1页面
    ((client_sp1 *)this->parent()->parent()->parent())->hide();
    //delete this->parent()->parent()->parent();
}
