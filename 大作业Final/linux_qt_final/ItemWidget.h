#ifndef ITEMWIDGET_H
#define ITEMWIDGET_H

#include <QWidget>

namespace Ui {
class CItemWidget;
}

class CItemWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CItemWidget(QWidget *parent = 0);
    ~CItemWidget();

    //设置数据
    void SetData(const QString& qstrFileName, int iFileSize, const QString& qstrPic);

private:
    Ui::CItemWidget *ui;

};

#endif // ITEMWIDGET_H
