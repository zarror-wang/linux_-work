#ifndef FILE_MEG_ITEM_ME_H
#define FILE_MEG_ITEM_ME_H

#include <QWidget>
#include <QTcpSocket>

namespace Ui {
class File_meg_item_me;
}

class File_meg_item_me : public QWidget
{
    Q_OBJECT

public:
    explicit File_meg_item_me(QWidget *parent = nullptr,QString clienter_name=nullptr, QString file_name=nullptr,QString meg_seq=nullptr,QTcpSocket* c_tcp=nullptr);
    ~File_meg_item_me();
    void init_fileitem(QImage* user_icon);//初始化

private slots:
    void on_download_clicked();

private:
    Ui::File_meg_item_me *ui;
    QString file_name;
    QString meg_seq;
    QString clienter_name;
    QTcpSocket* c_tcp;
};

#endif // FILE_MEG_ITEM_ME_H
