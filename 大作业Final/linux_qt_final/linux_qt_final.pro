QT       += core gui network testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    client_sp1.cpp \
    client_sp2.cpp \
    client_sp3.cpp \
    file_downloadpage.cpp \
    file_meg_item.cpp \
    file_meg_item_me.cpp \
    friend_info_dia.cpp \
    group_info_dia.cpp \
    groupitem.cpp \
    main.cpp \
    client_sp0.cpp \
    normal_meg_item.cpp \
    normal_meg_item_me.cpp \
    useritem.cpp

HEADERS += \
    client_sp0.h \
    client_sp1.h \
    client_sp2.h \
    client_sp3.h \
    client_tcp_module.h \
    file_downloadpage.h \
    file_meg_item.h \
    file_meg_item_me.h \
    friend_info_dia.h \
    group_info_dia.h \
    groupitem.h \
    normal_meg_item.h \
    normal_meg_item_me.h \
    useritem.h

FORMS += \
    client_sp0.ui \
    client_sp1.ui \
    client_sp2.ui \
    client_sp3.ui \
    file_downloadpage.ui \
    file_meg_item.ui \
    file_meg_item_me.ui \
    friend_info_dia.ui \
    group_info_dia.ui \
    groupitem.ui \
    normal_meg_item.ui \
    normal_meg_item_me.ui \
    useritem.ui

TRANSLATIONS += \
    linux_qt_final_zh_CN.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    QSS.qrc

DISTFILES += \
    ../sp0_1.png \
    ../下载/pear.png
