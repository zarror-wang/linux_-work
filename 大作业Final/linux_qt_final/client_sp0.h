#pragma once
#ifndef CLIENT_SP0_H
#define CLIENT_SP0_H
#include <QMainWindow>
#include <QScrollBar>
#include <QDebug>
#include<QTcpSocket>
#include<QHostAddress>
#include<QDebug>
#include "client_tcp_module.h"

QT_BEGIN_NAMESPACE
namespace Ui { class client_sp0; }
QT_END_NAMESPACE


class client_sp0 : public QMainWindow
{
    Q_OBJECT

public:
    client_sp0(QWidget *parent = nullptr);
    ~client_sp0();

private slots:
    void on_loadin_button_clicked();
    void fun_id_author(ack_str* ack);//处理服务器的返回信息

private:
    Ui::client_sp0 *ui;
    QTcpSocket* c_tcp;
    req_str* req;
    ack_str* ack;
    QString clienter_name;
    QString clienter_id;
};
#endif // CLIENT_SP0_H
