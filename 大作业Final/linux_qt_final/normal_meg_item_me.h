#ifndef NORMAL_MEG_ITEM_ME_H
#define NORMAL_MEG_ITEM_ME_H

#include <QWidget>

namespace Ui {
class normal_meg_item_me;
}

class normal_meg_item_me : public QWidget
{
    Q_OBJECT

public:
    explicit normal_meg_item_me(QWidget *parent = nullptr);
    ~normal_meg_item_me();
    void init_normalmegitem_me(QImage* user_icon,QString name,QString meg_con);

private:
    Ui::normal_meg_item_me *ui;
};

#endif // NORMAL_MEG_ITEM_ME_H
