#ifndef FILE_MEG_ITEM_H
#define FILE_MEG_ITEM_H

#include <QWidget>
#include <QTcpSocket>

namespace Ui {
class file_meg_item;
}

class file_meg_item : public QWidget
{
    Q_OBJECT

public:
    explicit file_meg_item(QWidget *parent = nullptr,QString u_name=nullptr,QString file_name=nullptr, QString meg_seq=nullptr,QTcpSocket* c_tcp=nullptr);
    ~file_meg_item();
    void init_fileitem(QImage* user_icon,QString name);

private slots:
    void on_download_clicked();

private:
    Ui::file_meg_item *ui;
    QString u_name;//发送方姓名
    QString meg_seq;//文件消息的序列号
    QString file_name;//文件名
    QTcpSocket* c_tcp;
};

#endif // FILE_MEG_ITEM_H
