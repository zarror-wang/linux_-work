#ifndef CLIENT_SP3_H
#define CLIENT_SP3_H

#include <QWidget>
#include <QTcpSocket>

namespace Ui {
class client_sp3;
}

class client_sp3 : public QWidget
{
    Q_OBJECT

public:
    explicit client_sp3(QWidget *parent = nullptr,QString clienter_id=nullptr,QString clienter_name=nullptr,QString group_name=nullptr,QString group_id=nullptr,int group_mem_cont=0,QVector<QString>* group_mem=nullptr,QTcpSocket* c_tcp=nullptr);
    ~client_sp3();
    void Update_Meg();//更新当前页面信息展示
    void fun_gen_meg();

private slots:
    void on_back2main_clicked();//返回sp1主页面

    void on_Send_Button_clicked();//发送群聊消息

    void on_send_file_clicked();

private:
    Ui::client_sp3 *ui;
    QString group_id;//记录当前聊天页面的群聊id
    QString group_name;//记录当前聊天页面的群聊name
    QString clienter_id;//记录当前聊天页面的主id
    QString clienter_name;//记录当前聊天页面的主name
    QVector<QString>* group_mem;//获取群聊成员
    int group_mem_cont;
    QTcpSocket* c_tcp;
};

#endif // CLIENT_SP3_H
