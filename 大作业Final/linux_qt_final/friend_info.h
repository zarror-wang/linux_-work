#ifndef FRIEND_INFO_H
#define FRIEND_INFO_H

#include <QWidget>

namespace Ui {
class Friend_Info;
}

class Friend_Info : public QWidget
{
    Q_OBJECT

public:
    explicit Friend_Info(QWidget *parent = nullptr);
    ~Friend_Info();

private:
    Ui::Friend_Info *ui;
};

#endif // FRIEND_INFO_H
