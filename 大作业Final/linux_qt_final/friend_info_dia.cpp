#include "friend_info_dia.h"
#include "ui_friend_info_dia.h"

friend_info_dia::friend_info_dia(QWidget *parent,QString u_id,QString u_name,QString u_motto) :
    QDialog(parent),
    u_id(u_id),
    u_name(u_name),
    u_motto(u_motto),
    ui(new Ui::friend_info_dia)
{
    ui->setupUi(this);
    QImage* img=new QImage;
    img->load("/home/zarror/usr_info_icon.png");
    ui->friend_icon ->setPixmap(QPixmap::fromImage(*img));
    ui->friend_id_val->setText(u_id);
    ui->friend_name_val->setText(u_name);
    ui->friend_motto_val->setText("德以明理，学以精工");
}

friend_info_dia::~friend_info_dia()
{
    delete ui;
}
