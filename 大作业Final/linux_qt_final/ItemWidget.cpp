#include "ItemWidget.h"
#include "ui_ItemWidget.h"

CItemWidget::CItemWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CItemWidget)
{
    ui->setupUi(this);
}

CItemWidget::~CItemWidget()
{
    delete ui;
}

void CItemWidget::SetData(const QString& qstrFileName, int iFileSize, const QString& qstrPic)
{
    ui->label_fileName->setText(qstrFileName);
    ui->label_fileSize->setText(QString::number(iFileSize));

    QPixmap pixmapPic(qstrPic);
    int iWidth = ui->label_pic->width();
    int iHeight = ui->label_pic->height();
    QPixmap pixmapPicFit = pixmapPic.scaled(iWidth, iHeight, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);//饱满填充
    ui->label_pic->setPixmap(pixmapPicFit);
}
