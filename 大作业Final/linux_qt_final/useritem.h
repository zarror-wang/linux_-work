#ifndef USERITEM_H
#define USERITEM_H

#include <QWidget>
#include <QTcpSocket>

namespace Ui {
class useritem;
}

class useritem : public QWidget
{
    Q_OBJECT

public:
    explicit useritem(QWidget *parent = nullptr,QString u_name=nullptr,QString u_id=nullptr,QString clienter_name=nullptr,QString clienter_id=nullptr,QTcpSocket* c_tcp=nullptr);
    void init_useritem(QImage* user_icon);
    ~useritem();

private slots:
    void on_to_info_detial_clicked();
    void on_to_meg_page_clicked();
private:
    Ui::useritem *ui;
    QString u_name;//该好友名称
    QString u_id;//该好友id
    QString clienter_name;//主名称
    QString clienter_id;//主id
    QTcpSocket* c_tcp;//链接参数
};

#endif // USERITEM_H
