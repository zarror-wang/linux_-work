#ifndef NORMAL_MEG_ITEM_H
#define NORMAL_MEG_ITEM_H

#include <QWidget>

namespace Ui {
class normal_meg_item;
}

class normal_meg_item : public QWidget
{
    Q_OBJECT

public:
    explicit normal_meg_item(QWidget *parent = nullptr);
    ~normal_meg_item();
    void init_normalmegitem(QImage* user_icon,QString name,QString meg_con);

private:
    Ui::normal_meg_item *ui;
};

#endif // NORMAL_MEG_ITEM_H
