#include "MainWidget.h"
#include "ui_MainWidget.h"
#include "ItemWidget.h"

CMainWidget::CMainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CMainWidget)
{
    ui->setupUi(this);

    ui->listWidget->setResizeMode(QListView::Adjust);
    ui->listWidget->setViewMode(QListView::IconMode);

    AddItem("Video1", 1024, ":/images/video_1.png");
    AddItem("Video2", 2048, ":/images/video_2.png");
    AddItem("Video3", 3072, ":/images/video_3.png");
    AddItem("Video4", 4096, ":/images/video_4.png");
    AddItem("Video5", 5120, ":/images/video_1.png");
}

CMainWidget::~CMainWidget()
{
    delete ui;
}

void CMainWidget::AddItem(const QString& qstrFileName, int iFileSize, const QString& qstrPic)
{
    CItemWidget* pItemWidget = new CItemWidget(this);
    pItemWidget->SetData(qstrFileName, iFileSize, qstrPic);
    QListWidgetItem* pItem = new QListWidgetItem();
    pItem->setSizeHint(QSize(350, 40));
    ui->listWidget->addItem(pItem);
    ui->listWidget->setItemWidget(pItem, pItemWidget);
}
