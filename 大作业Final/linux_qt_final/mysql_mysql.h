#ifndef MYSQL_MYFUN_H
#define MYSQL_MYFUN_H
#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <mysql/mysql.h>
#include <vector>
#include <string>
#include <map>

class Mysql_mysql{
private:
MYSQL* mysql_link;
private:
void display_row(MYSQL *mysql_main, MYSQL_ROW sqlrow);
MYSQL *Connection_To_Sql(MYSQL *conn_ptr);
std::string Get_inner_Id(MYSQL *conn_ptr, std::string User_Id);
void Get_Mate_Info(MYSQL *conn_ptr, std::string Member_Id, std::map<std::string, std::string> &Rela_map);
void Get_Group_Info_main(MYSQL *conn_ptr, std::string User_Id, std::map<std::pair<std::string, std::string>, std::vector<std::string>> &Group_map);
void Get_Group_Info_sub(MYSQL *conn_ptr, std::vector<std::string> Group_UserIn, std::map<std::pair<std::string, std::string>, std::vector<std::string>> &Group_map);
void Close_Connect_To_Sql(MYSQL *conn_ptr);
};
#endif // MYSQL_MYFUN_H
