#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <mysql/mysql.h>
#include <vector>
#include <string>
#include <map>

void display_row(MYSQL *mysql_main, MYSQL_ROW sqlrow);
MYSQL *Connection_To_Sql(MYSQL *conn_ptr);
std::string Get_inner_Id(MYSQL *conn_ptr, std::string User_Id);
void Get_Mate_Info(MYSQL *conn_ptr, std::string Member_Id, std::map<std::string, std::string> &Rela_map);
void Get_Group_Info_main(MYSQL *conn_ptr, std::string User_Id, std::map<std::pair<std::string, std::string>, std::vector<std::string>> &Group_map);
void Get_Group_Info_sub(MYSQL *conn_ptr, std::vector<std::string> Group_UserIn, std::map<std::pair<std::string, std::string>, std::vector<std::string>> &Group_map);
void Close_Connect_To_Sql(MYSQL *conn_ptr);

void display_row(MYSQL *mysql_main, MYSQL_ROW sqlrow)
{
    unsigned int field_count;

    field_count = 0;
    while (field_count < mysql_field_count(mysql_main))
    {
        if (sqlrow[field_count])
            printf("%s ", sqlrow[field_count]);
        else
            printf("NULL");
        field_count++;
    }
    printf("\n");
}

MYSQL *Connection_To_Sql(MYSQL *conn_ptr) //连接数据库服务器茶
{
    unsigned int timeout = 7; //超时时间7秒
    int ret = 0;
    conn_ptr = mysql_init(NULL); //初始化
    if (!conn_ptr)
    {
        printf("mysql_init failed!\n");
    }

    ret = mysql_options(conn_ptr, MYSQL_OPT_CONNECT_TIMEOUT, (const char *)&timeout); //设置超时选项
    if (ret)
    {
        printf("Options Set ERRO!\n");
    }
    conn_ptr = mysql_real_connect(conn_ptr, "localhost", "root", "root", "Server_Msg_Info", 3306, NULL, 0); //连接MySQL testdb数据库
    if (conn_ptr)
    {
        printf("Connection Succeed!\n");
        //ret = mysql_query(conn_ptr, "SELECT * FROM User_Info WHERE User_Inner_Id > 1"); //执行SQL语句
        // if (!ret)
        // {
        //     res_ptr = mysql_use_result(conn_ptr);
        //     if (res_ptr)
        //     {
        //         printf("Retrieved %lu rows\n", (unsigned long)mysql_num_rows(res_ptr)); //在结果集合中返回行的数量
        //         while ((sqlrow = mysql_fetch_row(res_ptr)))                             //返回store_result中得到的结构体，并从中检索单行
        //         {
        //             printf("Fetched data\n");
        //             display_row(conn_ptr,sqlrow);
        //         }
        //     }

        //     if (mysql_errno(conn_ptr))
        //     {
        //         printf("Connect Erro:%d %s\n", mysql_errno(conn_ptr), mysql_error(conn_ptr)); //返回错误代码、错误消息
        //         return -2;
        //     }

        //     mysql_free_result(res_ptr);
        // }
        // else
        // {
        //     printf("Connect Erro:%d %s\n", mysql_errno(conn_ptr), mysql_error(conn_ptr)); //返回错误代码、错误消息
        //     return -3;
        // }
        // mysql_close(conn_ptr);
        // printf("Connection closed!\n");
    }
    else //错误处理
    {
        printf("Connection Failed!\n");
        if (mysql_errno(conn_ptr))
        {
            printf("Connect Erro:%d %s\n", mysql_errno(conn_ptr), mysql_error(conn_ptr)); //返回错误代码、错误消息
        }
    }

    return conn_ptr;
};

std::string Get_inner_Id(MYSQL *conn_ptr, std::string User_Id) //查找用户的内部ID
{
    MYSQL_RES *res_ptr;
    MYSQL_ROW sqlrow;
    int ret = 0;
    std::string inner_id = "";
    std::string sel_ptr = "SELECT User_Inner_Id FROM User_Info WHERE User_Id=";
    sel_ptr += User_Id;
    ret = mysql_query(conn_ptr, (char *)sel_ptr.c_str());
    if (!ret)
    {
        res_ptr = mysql_use_result(conn_ptr);
        if (res_ptr)
        {
            printf("Retrieved %lu rows\n", (unsigned long)mysql_num_rows(res_ptr)); //在结果集合中返回行的数量
            while ((sqlrow = mysql_fetch_row(res_ptr)))                             //返回store_result中得到的结构体，并从中检索单行
            {
                //printf("Fetched data\n");
                //display_row(conn_ptr, sqlrow);
                inner_id = sqlrow[0];
            }
        }

        if (mysql_errno(conn_ptr))
        {
            printf("Connect Erro:%d %s\n", mysql_errno(conn_ptr), mysql_error(conn_ptr)); //返回错误代码、错误消息
        }

        mysql_free_result(res_ptr);
    }
    else
    {
        printf("Connect Erro:%d %s\n", mysql_errno(conn_ptr), mysql_error(conn_ptr)); //返回错误代码、错误消息
    }
    return inner_id;
}

void Get_Mate_Info(MYSQL *conn_ptr, std::string Member_Id, std::map<std::string, std::string> &Rela_map) //获取好友列表
{
    MYSQL_RES *res_ptr;
    MYSQL_ROW sqlrow;
    int ret = 0;
    std::string inner_id = "";
    inner_id = Get_inner_Id(conn_ptr, Member_Id);
    std::string sel_ptr = "SELECT * FROM User_Info WHERE User_Inner_Id in (SELECT B_Id FROM Relationship_Info WHERE A_Id=";
    sel_ptr += inner_id;
    sel_ptr += ")";
    ret = mysql_query(conn_ptr, (char *)sel_ptr.c_str()); //执行SQL语句
    if (!ret)
    {
        res_ptr = mysql_use_result(conn_ptr);
        if (res_ptr)
        {
            printf("Retrieved %lu rows\n", (unsigned long)mysql_num_rows(res_ptr)); //在结果集合中返回行的数量
            while ((sqlrow = mysql_fetch_row(res_ptr)))                             //返回store_result中得到的结构体，并从中检索单行
            {
                //printf("Fetched data\n");
                //display_row(conn_ptr, sqlrow);
                Rela_map[sqlrow[1]] = sqlrow[3];
            }
        }

        if (mysql_errno(conn_ptr))
        {
            printf("Connect Erro:%d %s\n", mysql_errno(conn_ptr), mysql_error(conn_ptr)); //返回错误代码、错误消息
        }

        mysql_free_result(res_ptr);
    }
    else
    {
        printf("Connect Erro:%d %s\n", mysql_errno(conn_ptr), mysql_error(conn_ptr)); //返回错误代码、错误消息
    }
}

void Get_Group_Info_main(MYSQL *conn_ptr, std::string User_Id, std::map<std::pair<std::string, std::string>, std::vector<std::string>> &Group_map) //获取用户参加的群聊
{
    MYSQL_RES *res_ptr;
    MYSQL_ROW sqlrow;
    int ret = 0;
    std::vector<std::string> Group_UserIn;
    std::pair<std::string, std::string> temp_pair;
    std::string inner_id = "";
    inner_id = Get_inner_Id(conn_ptr, User_Id);
    std::string sel_ptr = "SELECT Group_Id FROM User_Group_Relation WHERE User_Inner_Id =";
    sel_ptr += inner_id;
    ret = mysql_query(conn_ptr, (char *)sel_ptr.c_str()); //执行SQL语句
    if (!ret)
    {
        res_ptr = mysql_use_result(conn_ptr);
        if (res_ptr)
        {
            printf("Retrieved %lu rows\n", (unsigned long)mysql_num_rows(res_ptr)); //在结果集合中返回行的数量
            while ((sqlrow = mysql_fetch_row(res_ptr)))                             //返回store_result中得到的结构体，并从中检索单行
            {
                //printf("Fetched data\n");
                //display_row(conn_ptr, sqlrow);
                Group_UserIn.push_back(sqlrow[0]);
            }
        }
        mysql_free_result(res_ptr);
    }
    Get_Group_Info_sub(conn_ptr, Group_UserIn, Group_map);

}

void Get_Group_Info_sub(MYSQL *conn_ptr, std::vector<std::string> Group_UserIn, std::map<std::pair<std::string, std::string>, std::vector<std::string>> &Group_map) //获取用户的群组信息
{
    MYSQL_RES *res_ptr;
    MYSQL_ROW sqlrow;
    int ret = 0;
    std::pair<std::string, std::string> temp_pair;
    std::string sel_ptr = "SELECT * FROM User_Group_Relation WHERE Group_Id =";

    for (int i = 0; i < Group_UserIn.size(); i++)
    {
        sel_ptr += Group_UserIn[i];
        ret = mysql_query(conn_ptr, (char *)sel_ptr.c_str()); //执行SQL语句
        if (!ret)
        {
            res_ptr = mysql_use_result(conn_ptr);
            if (res_ptr)
            {
                printf("Retrieved %lu rows\n", (unsigned long)mysql_num_rows(res_ptr)); //在结果集合中返回行的数量
                while ((sqlrow = mysql_fetch_row(res_ptr)))                             //返回store_result中得到的结构体，并从中检索单行
                {
                    //printf("Fetched data\n");
                    display_row(conn_ptr, sqlrow);
                    temp_pair.first = sqlrow[1];
                    temp_pair.second = sqlrow[2];
                    Group_map[temp_pair].push_back(sqlrow[3]);
                }
            }
            mysql_free_result(res_ptr);
        }
    }
}

void Close_Connect_To_Sql(MYSQL *conn_ptr)
{
    mysql_close(conn_ptr);
    std::cout << "successfully close" << std::endl;
}
