#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>

using namespace std;
int Get_Matrix(int M[][1500])
{
    fstream file;
    string line_content = "";
    file.open("2020X2020.txt");
    int line_index = 0;
    getline(file, line_content);
    while (getline(file, line_content))
    {
        int index = 0;
        for (int i = 0; i < line_content.length(); i++)
        {
            if (line_content[i] != ',')
            {
                M[line_index][index] = line_content[i] - '0';
                index++;
            }
        }
        line_index++;
    }
    // for (int i = 0; i < 10; i++)
    // {
    //     for (int j = 0; j < 10; j++)
    //         cout << M[i][j] << "  ";
    //     cout << endl;
    // }
    // cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl;
    file.close();
}

void Write_File(int M[][1500], int horline, int verline)
{
    string name = "";
    name += to_string(horline);
    name += "x";
    name += to_string(verline);
    name += ".txt";
    ofstream file;
    file.open(name.c_str());
    for (int i = 0; i < horline; i++)
    {
        for (int j = 0; j < verline; j++)
            file << M[i][j] << ",";
        file << '\n';
    }
    file.close();
    return;
}
