文件类型----------文件名----------内含文件-----------------功能------------------------------
shell文件          1.sh                               实现输入整数N，并按N为时间间隔计时
--------------------------------------------------------------------------------------------
shell文件          2.sh                               将该文件放在搜索的同级目录下，运行后，
                                                      输入要查找的字符串，便会递归搜索并显示。
--------------------------------------------------------------------------------------------
shell文件          3.sh                               显示当前目录下的文件夹
--------------------------------------------------------------------------------------------
folder             4             4.cpp
				  4.sh
				  OlderFolder文件夹     
				  README.md             详细内容参考内部README.md文件
-------------------------------------------------------------------------------------------
folder		    5            参考5内README.md文件   详细内容参考内部README.md文件
