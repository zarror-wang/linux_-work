本实验采用C语言是实现Shell的各种命令
	cd 转换目录
	ls 显示当前文件夹内容
	pwd 显示当前绝对路径
	rm 删除文件夹或文件
	mv 移动文件夹或文件
	mkdir 新建一个文件夹
	cp 复制文件夹或文件
	exit 退出当前Shell
	history 显示用户的历史命令
标准命令格式：
             操作名称                                      示例格式
---------------------------------------------------------------------------------------
	cd 目标文件夹路径				       cd /usr/local
	ls							ls
	pwd							pwd
	rm 目标删除的文件或文件夹				rm filename.cpp
	mv 源文件或文件夹 目标文件夹路径			mv folderA/1.cpp   folderB/2.cpp    
	mkdir 目标文件夹名					mkdir folderC
	cp  源文件或文件夹 目标文件夹路径			cp folderA/1.cpp   folderB/2.cpp 
	exit							exit
	history						history
