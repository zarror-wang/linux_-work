#include<stdio.h>
#include<dlfcn.h>
#define SO_FILE "./libmy.so"

int main()
{
  void* sfp;
  char* err;
  int num=10;
  int ans=0;
  char temp[50]="Hello world,this is zarror!";
  char temp1[50]="Hello world,this is zarror!";
  int (*sub_fun1)(int);
  void (*sub_fun2)(char*);
  sfp=dlopen(SO_FILE,RTLD_LAZY);
  if(sfp==NULL)
  {
  fprintf(stderr,dlerror());
  exit(1);
  }
  sub_fun1=dlsym(sfp,"sub_fun1");
  err=dlerror();
  if(err)
  {
  printf(stderr,err);
  exit(2);
  }
  
  sub_fun2=dlsym(sfp,"sub_fun2");
  err=dlerror();
  if(err)
  {
  printf(stderr,err);
  exit(3);
  }
  
  printf("----------------------------调用函数一\n");
  ans=sub_fun1(num);
  printf("%d的阶乘为：%d\n",num,ans);
  printf("----------------------------调用函数二\n");
  sub_fun2(temp);
  printf("原始字符串为：%s\n",temp1);
  printf("替换后的字符串为：%s\n",temp);
  dlclose(sfp);
  exit(0);
}
