#!/bin/bash

mkdir a
mkdir b
cd a
ls
#创建文件
touch 1.c
touch 2.c
touch 3.txt
touch 4.sh
touch 5.c
touch 6.c
#写文件
echo ""
echo "此时文件夹a中的文件有："
ls
echo ""
con_1="This is the content for 1.c,here comes some verbs.run chase paly and sing!"
con_2="This is the content in 2.c,it's just for testing the proc!"
con_3="This is what you wile see in the 3.txt,lucky for you!"
con_4="This is the contenet in 4.sh,it's a powershell script"
con_5="This is 5.c"
con_6="Here is the file 6.c,it,s the final one"

echo $con_1 > 1.c
echo $con_2 > 2.c
echo $con_3 > 3.txt
echo $con_4 > 4.sh
echo $con_5 > 5.c
echo $con_6 > 6.c

echo "完成文件内容写入！"
echo ""
echo "目前文件夹a中的文件具体内容为："
echo "<------------------------------------1.c----------------------------------->"
cat 1.c
echo "<------------------------------------2.c----------------------------------->"
cat 2.c
echo "<------------------------------------3.txt--------------------------------->"
cat 3.txt
echo "<------------------------------------4.sh---------------------------------->"
cat 4.sh
echo "<------------------------------------5.c----------------------------------->"
cat 5.c
echo "<------------------------------------6.c----------------------------------->"
cat 6.c

echo ""
#文件复制
cd ..
cd b
echo "此时文件夹b中的文件有"
ls
echo ""
echo "现在将文件夹a中的*.c文件移入b文件夹"
cd ..
cd a
for item in "*.c" 
do
	cp $item ../b
done
echo ""
echo "完成文件转移，此时文件夹b中的文件有："
cd ..
cd b
ls -Slh
echo ""

































